export default {
  name: "App",

  components: {
    SearchForm: () => import("@/components/SearchForm/SearchForm.vue"),
    SearchResult: () => import("@/components/SearchResult/SearchResult.vue")
  }
};
