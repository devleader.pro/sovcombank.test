import Vue from "vue";
import vSelect from "vue-select";

Vue.config.productionTip = false;
Vue.component("v-select", vSelect);
/* eslint-disable no-new */
new Vue({
  el: "#app",
  components: { App: () => import("@/layout/App/App.vue") },
  template: "<App/>",
  data() {
    return {
      result: null
    };
  }
});
