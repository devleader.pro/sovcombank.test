export default {
  name: "SearchForm",
  data() {
    return {
      advencedSearch: true,
      price: false,
      space: false
    };
  },
  components: {
    SearchByName: () => import("@/components/SearchByName/SearchByName.vue"),
    SearchByParams: () =>
      import("@/components/SearchByParams/SearchByParams.vue")
  }
};
