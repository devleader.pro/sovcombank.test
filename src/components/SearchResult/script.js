export default {
  name: "SearchForm",
  data() {
    return {
      typeOfSearch: "byName"
    };
  },
  components: {
    SearchByName: () => import("@/components/SearchByName/SearchByName.vue"),
    SearchByParams: () =>
      import("@/components/SearchByParams/SearchByParams.vue")
  }
};
