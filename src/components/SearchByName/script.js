export default {
  name: "SearchFormByName",
  data() {
    return {
      searchString: null
    };
  },
  methods: {
    searchResults() {
      this.$root.result = { searchString: this.searchString };
    }
  }
};
