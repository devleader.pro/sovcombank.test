export default {
  name: "SearchFormByParams",
  data() {
    return {
      formData: {
        city: "Москва",
        action: "Купить",
        type: "Офис",
        minPrice: null,
        maxPrice: null,
        curency: "₽/месяц",
        minSpace: null,
        maxSpace: null
      },
      attributesForSelect: {
        ref: "openIndicator",
        role: "presentation",
        class: "vs__open-indicator"
      }
    };
  },
  methods: {
    searchResults() {
      const formData = this.formData;
      const result = {
        city: formData.city,
        action: formData.action,
        type: formData.type
      };
      if (formData.minPrice) {
        result.minPrice = formData.minPrice;
      }
      if (formData.maxPrice) {
        result.maxPrice = formData.maxPrice;
      }
      if (result.minPrice || result.maxPrice) {
        result.curency = formData.curency;
      }
      if (formData.minSpace) {
        result.minSpace = formData.minSpace;
      }
      if (formData.maxPrice) {
        result.maxSpace = formData.maxSpace;
      }
      this.$root.result = result;
    }
  },
  watch: {
    ["$parent.space"]: function() {
      if (!this.$parent.space) {
        this.minSpace = null;
        this.maxSpace = null;
      }
    },
    ["$parent.price"]: function() {
      if (!this.$parent.price) {
        this.minPrice = null;
        this.maxPrice = null;
        this.curency = "₽/месяц";
      }
    }
  }
};
